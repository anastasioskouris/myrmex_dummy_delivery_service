import sys
from myrmex_dummy_delivery_service.window import Window
from myrmex_dummy_delivery_service.ros_node import RosNode
from PyQt4 import QtGui

def run():
    app = QtGui.QApplication(sys.argv)
    w = Window()
    ros_node = RosNode()
    w.start(ros_node)
    return app.exec_()
