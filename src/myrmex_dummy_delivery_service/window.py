from myrmex_dummy_delivery_service.ui import delivery_service_ui
from PyQt4 import QtGui, QtCore
from PyQt4.QtCore import pyqtSlot, pyqtSignal


class Window(QtGui.QWidget):
    MAX_DIGITS = 19
    _order_received = pyqtSignal()

    def __init__(self, parent=None):
        super(Window, self).__init__(parent=parent)

        self._ui = delivery_service_ui.Ui_Form()
        self._ui.setupUi(self)
        self._worker = None

        # Initialize MainWindow
        self._ui.pushButton0.clicked.connect(
                lambda: self._number_button_pressed(0)
        )
        self._ui.pushButton1.clicked.connect(
                lambda: self._number_button_pressed(1)
        )
        self._ui.pushButton2.clicked.connect(
                lambda: self._number_button_pressed(2)
        )
        self._ui.pushButton3.clicked.connect(
                lambda: self._number_button_pressed(3)
        )
        self._ui.pushButton4.clicked.connect(
                lambda: self._number_button_pressed(4)
        )
        self._ui.pushButton5.clicked.connect(
                lambda: self._number_button_pressed(5)
        )
        self._ui.pushButton6.clicked.connect(
                lambda: self._number_button_pressed(6)
        )
        self._ui.pushButton7.clicked.connect(
                lambda: self._number_button_pressed(7)
        )
        self._ui.pushButton8.clicked.connect(
                lambda: self._number_button_pressed(8)
        )
        self._ui.pushButton9.clicked.connect(
                lambda: self._number_button_pressed(9)
        )
        self._ui.confirmButton.clicked.connect(self._confirm_pressed)
        self._ui.clearButton.clicked.connect(self._clear_pressed)
        self._ui.advancedOptionsButton.clicked.connect(
            self._advanced_options_pressed
        )

        # Initialize wrong order number window
        self._ui.okButton.clicked.connect(self._ok_pressed)

        # Initialize waiting page
        self._movie = QtGui.QMovie(":/icons/waiting.gif", QtCore.QByteArray(),
                                   self._ui.waitingPageLabel)
        self._movie.setCacheMode(QtGui.QMovie.CacheAll)
        self._movie.setSpeed(300)
        self._ui.waitingPageLabel.setMovie(self._movie)
        self._order_received.connect(self._on_delivery_received)

        # Initialize pick up confirm page
        self._ui.confirmPickUpButton.clicked.connect(self._pick_up_confirmed)

        # Initialize thank you page
        self._timer = QtCore.QTimer()
        self._timer.timeout.connect(self._timer_expired)

        # Initialize advanced options page
        self._ui.goBackButton.clicked.connect(self._on_go_back)
        self._ui.openDoorButton.clicked.connect(self._on_open_door)
        self._ui.closeDoorButton.clicked.connect(self._on_close_door)

        self._go_main_page()

    def _get_big_label_text(self):
		return str(self._ui.bigLabel.text())

    def _set_big_label_text(self, plain_text):
		self._ui.bigLabel.setText(plain_text)

    def _get_small_label_text(self):
		return str(self._ui.instructionLabel.text())

    def _set_small_label_text(self, plain_text):
		self._ui.instructionLabel.setText(plain_text)

    def _get_display_text(self):
        return str(self._ui.displayLabel.text())

    def _set_display_text(self, plain_text):
        self._ui.displayLabel.setText(plain_text)

    def _number_button_pressed(self, number):
        tmp_number = self._get_display_text()
        if len(tmp_number) == self.MAX_DIGITS:
            return
        if not tmp_number:
            self._set_display_text(str(number))
        elif len(''.join(tmp_number.split('-'))) % 4 == 0:
            self._set_display_text('%s-%s' % (tmp_number, str(number)))
        else:
            self._set_display_text('%s%s' % (tmp_number, str(number)))

    @pyqtSlot()
    def _confirm_pressed(self):
        order_string = self._get_display_text()
        order_number = ''.join(order_string.split('-'))
        if not self._check_order(order_number):
            self._go_wrong_order_page()
            return
        else:
            self._go_waiting_page()
            self._bring_order(order_number)

    @pyqtSlot()
    def _clear_pressed(self):
        self._set_display_text('')

    @pyqtSlot()
    def _advanced_options_pressed(self):
        self._go_advanced_options_page(preserve=True)

    def on_delivery_received(self):
        self._order_received.emit()

    @pyqtSlot()
    def _on_delivery_received(self):
        self._go_pickup_confirm_page()
        self._on_open_door()

    @pyqtSlot()
    def _ok_pressed(self):
        self._go_main_page()

    @pyqtSlot()
    def _pick_up_confirmed(self):
        self._go_thank_you_page()
        self._on_close_door()

    @pyqtSlot()
    def _timer_expired(self):
        self._timer.stop()
        self._go_main_page()

    @pyqtSlot()
    def _on_go_back(self):
        self._go_main_page(preserve=True)

    @pyqtSlot()
    def _on_open_door(self):
        self._worker.set_delivery_door_state(True)

    @pyqtSlot()
    def _on_close_door(self):
        self._worker.set_delivery_door_state(False)

    def _go_main_page(self, preserve=False):
        self._set_big_label_text('Please input your order code')
        self._set_small_label_text('')
        if not preserve:
            self._set_display_text('')
        self._movie.stop()
        self._ui.stackedWidget.setCurrentIndex(0)

    def _go_wrong_order_page(self):
        self._set_big_label_text('Order cannot be found')
        self._set_small_label_text('Please try again')
        self._set_display_text('')
        self._movie.stop()
        self._ui.stackedWidget.setCurrentIndex(1)

    def _go_waiting_page(self):
        self._set_big_label_text('We are fetching your order')
        self._set_small_label_text('Please wait')
        self._set_display_text('')
        self._movie.start()
        self._ui.stackedWidget.setCurrentIndex(2)

    def _go_pickup_confirm_page(self):
        self._set_big_label_text('Please receive your order items')
        self._set_small_label_text('')
        self._set_display_text('')
        self._movie.stop()
        self._ui.stackedWidget.setCurrentIndex(3)

    def _go_thank_you_page(self):
        self._set_big_label_text('Thank you for choosing us')
        self._set_small_label_text('')
        self._set_display_text('')
        self._movie.stop()
        self._ui.stackedWidget.setCurrentIndex(4)
        self._timer.start(5000)

    def _go_advanced_options_page(self, preserve=True):
        self._set_big_label_text('')
        self._set_small_label_text('')
        if not preserve:
            self._set_display_text('')
        self._movie.stop()
        self._ui.stackedWidget.setCurrentIndex(5)

    def closeEvent(self, *args, **kwargs):
        if self._worker:
            self._worker.stop()
        super(Window, self).closeEvent(*args, **kwargs)

    def start(self, worker):
        self._worker = worker
        self._worker.start(self.on_delivery_received)
        self.showFullScreen()


    def _check_order(self, ok=True):
        if ok:
            print('supposing order number is ok')
            return True
        print('supposing order number is not ok')
        return False

    def _bring_order(self, order_number):
        print('Bring my order and tell me once its here!')
        if not self._worker.notify():
            print('Something is wrong!')

